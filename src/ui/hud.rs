use bevy::prelude::*;

use crate::Money;

use super::UiAssets;

pub struct HudPlugin<S> {
    running: S,
}

impl<S> HudPlugin<S> {
    pub fn new(running: S) -> Self {
        Self { running }
    }
}

impl<S: States + Clone> Plugin for HudPlugin<S> {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(self.running.clone()), setup_hud)
            .add_systems(OnExit(self.running.clone()), teardown_hud)
            .add_systems(
                Update,
                (update_money_ui).run_if(in_state(self.running.clone())),
            );
    }
}

#[derive(Debug, Component)]
struct MoneyText;
#[derive(Debug, Component)]
struct Hud;

fn setup_hud(mut commands: Commands, ui_assets: Res<UiAssets>) {
    let Some(font) = ui_assets.fonts.get("fonts/BalooThambi-Regular.ttf") else {
        return;
    };

    commands.spawn((
        TextBundle::from_section(
            "000",
            TextStyle {
                font: font.clone(),
                font_size: 60.0,
                color: Color::DARK_GREEN,
            },
        )
        .with_style(Style {
            position_type: PositionType::Absolute,
            ..default()
        }),
        MoneyText,
        Name::new("Money"),
    ));
}
fn teardown_hud() {}
fn update_money_ui(mut ui: Query<&mut Text, With<MoneyText>>, money: Option<Res<Money>>) {
    let money = money.map(|money| money.0).unwrap_or_default();

    for mut text in &mut ui {
        text.sections[0].value = format!("{money:.2}$$");
    }
}
