use bevy::prelude::*;
use bevy_asset_loader::prelude::{LoadingStateAppExt, *};
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_mod_picking::prelude::*;

mod level;
mod mob;
mod tower;
mod ui;

fn main() {
    App::new()
        .add_plugins((
            DefaultPlugins
                //
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        position: WindowPosition::Centered(MonitorSelection::Index(1)),
                        ..default()
                    }),
                    ..default()
                })
                //
                .set(AssetPlugin {
                    mode: AssetMode::Processed,
                    ..default()
                }),
            DefaultPickingPlugins,
            level::LevelPlugin::new(State::Loading, State::Loaded),
            tower::TowerPlugin::new(State::Loading, State::Loaded),
            mob::MobsPlugin::new(State::Loading, State::Loaded),
            ui::UiPlugin::new(State::Loading, State::Loaded),
        ))
        .register_type::<GeneralAssets>()
        .add_state::<State>()
        .add_event::<AddMoneyEvent>()
        .add_loading_state(LoadingState::new(State::Loading).continue_to_state(State::Loaded))
        .add_systems(Startup, startup)
        .add_systems(
            Update,
            (
                add_money,
                despawn_timer,
                (
                    physics_marker,
                    apply_velocity,
                    start_scaling,
                    (update_viewport, apply_deferred),
                    scale_world,
                )
                    .chain(),
            ),
        )
        //
        .add_plugins((
            bevy_inspector_egui::DefaultInspectorConfigPlugin,
            WorldInspectorPlugin::new(),
        ))
        //
        .add_collection_to_loading_state::<_, GeneralAssets>(State::Loading)
        .run();
}

fn physics_marker() {}
fn start_scaling() {}

#[derive(Debug, Resource, Reflect, Default, AssetCollection)]
#[reflect(Resource)]
struct GeneralAssets {
    #[asset(path = "images/towerDefense_tilesheet.png")]
    #[asset(texture_atlas(
        tile_size_x = 62.,
        tile_size_y = 62.,
        columns = 23,
        rows = 13,
        offset_x = 1.0,
        offset_y = 1.0,
        padding_x = 2.0,
        padding_y = 2.0,
    ))]
    atlas: Handle<TextureAtlas>,
}

#[derive(Debug, Resource)]
struct ViewPort(Vec2, Vec2);

const TARGET_ASPECT_RATIO: f32 = 16.0 / 9.0;

fn update_viewport(
    cameras: Query<&OrthographicProjection, With<Camera>>,
    view_port: Option<ResMut<ViewPort>>,
    mut commands: Commands,
    selected_level: Option<Res<LoadedLevel>>,
) {
    let Ok(camera) = cameras.get_single() else {
        warn!("Multiple Cameras are not supposed to exist!");
        return;
    };

    let camera_area = camera.area;
    let area = Rect::new(
        camera_area.min.x + 20.0,
        camera_area.min.y + 0.0,
        camera_area.max.x - 100.0,
        camera_area.max.y - 0.0,
    );

    let aspect = area.width() / area.height();
    let area = if aspect > TARGET_ASPECT_RATIO {
        let width = area.height() * TARGET_ASPECT_RATIO;
        Rect::from_center_size(area.center(), Vec2::new(width, area.height()))
    } else {
        let height = area.width() / TARGET_ASPECT_RATIO;

        Rect::from_center_size(area.center(), Vec2::new(area.width(), height))
    };

    let center = area.center();
    let scale = selected_level
        .map(|s| area.size() / s.size)
        .unwrap_or(Vec2::ONE);

    if let Some(mut v) = view_port {
        v.0 = center;
        v.1 = scale;
    } else {
        commands.insert_resource(ViewPort(center, scale));
    }
}

#[derive(Debug, States, Default, Hash, Clone, PartialEq, Eq)]
enum State {
    #[default]
    Loading,
    Loaded,
}

fn startup(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

#[derive(Debug, Component)]
struct LevelRoot;

#[derive(Debug, Component)]
struct LevelOffset;

fn scale_world(
    view_port: Option<Res<ViewPort>>,
    mut level: Query<&mut Transform, With<LevelRoot>>,
) {
    let Some(view_port) = view_port else {
        return;
    };

    let mut transform = match level.get_single_mut() {
        Ok(transform) => transform,
        Err(bevy::ecs::query::QuerySingleError::MultipleEntities(_)) => {
            warn!("Multiple Levelroots!");
            return;
        }
        Err(_) => {
            return;
        }
    };
    let mut pos = view_port.0;
    pos.y = -pos.y;

    transform.translation = (pos).extend(0.0);
    transform.scale = view_port.1.extend(1.0);
}

#[derive(Debug, Resource)]
struct LoadedLevel {
    bg_image: Handle<Image>,
    path: Vec<Vec2>,
    size: Vec2,
    grids: Vec<Vec2>,
    grid_scale: f32,
    damage: f32,
}

#[derive(Debug, Component)]
struct LevelEntity;

#[derive(Debug, Resource)]
struct Money(u32);

#[derive(Debug, Event)]
struct AddMoneyEvent(u32);

#[derive(Component)]
struct Velocity(Vec2);

#[derive(Component)]
struct Despawn(f32);

#[derive(Component)]
struct Collider(Shape);

enum Shape {
    Ball(parry2d::shape::Ball),
    Capsule(parry2d::shape::Capsule),
}

impl Collider {
    fn ball(radius: f32) -> Self {
        Self(Shape::Ball(parry2d::shape::Ball::new(radius)))
    }
    fn capsule(half_height: f32, radius: f32) -> Self {
        Self(Shape::Capsule(parry2d::shape::Capsule::new_y(
            half_height,
            radius,
        )))
    }

    fn shape(&self) -> &dyn parry2d::shape::Shape {
        match &self.0 {
            Shape::Ball(s) => s,
            Shape::Capsule(s) => s,
        }
    }
}

fn apply_velocity(mut query: Query<(&Velocity, &mut Transform)>, time: Res<Time>) {
    for (Velocity(v), mut transform) in query.iter_mut() {
        transform.translation += v.extend(0.0) * time.delta_seconds();
    }
}

fn despawn_timer(
    mut query: Query<(Entity, &mut Despawn)>,
    time: Res<Time>,
    mut commands: Commands,
) {
    for (entity, mut despawn) in query.iter_mut() {
        if despawn.0 < time.delta_seconds() {
            commands.entity(entity).despawn_recursive();
        }
        despawn.0 -= time.delta_seconds();
    }
}

fn add_money(
    mut commands: Commands,
    money: Option<ResMut<Money>>,
    mut money_events: EventReader<AddMoneyEvent>,
) {
    let mut added = 0;
    for e in money_events.read() {
        added += e.0;
    }

    if added > 0 {
        if let Some(mut money) = money {
            money.0 = money.0.saturating_add(added);
        } else {
            commands.insert_resource(Money(added))
        }
    }
}
