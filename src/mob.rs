use bevy::{prelude::*, reflect::TypeUuid, utils::HashMap};
use bevy_asset_loader::prelude::*;
use bevy_common_assets::ron::RonAssetPlugin;
use serde::Deserialize;

use crate::{
    apply_velocity, AddMoneyEvent, Collider, GeneralAssets, LevelEntity, LevelOffset, LoadedLevel,
    Velocity,
};

pub struct MobsPlugin<S> {
    loading: S,
    running: S,
}

impl<S> MobsPlugin<S> {
    pub fn new(loading: S, running: S) -> Self {
        Self { loading, running }
    }
}
impl<S: States + Clone> Plugin for MobsPlugin<S> {
    fn finish(&self, app: &mut App) {
        app.add_collection_to_loading_state::<_, MobInfos>(self.loading.clone());
    }

    fn build(&self, app: &mut App) {
        app.add_plugins(RonAssetPlugin::<Difficulty>::new(&["difficulty.ron"]))
            .add_event::<SpawnMob>()
            .add_event::<SpawnWave>()
            .register_type::<MobInfos>()
            .register_type::<WaveTimer>()
            .register_type::<Speed>()
            .register_type::<PathDistance>()
            .register_type::<Difficulty>()
            .add_systems(OnExit(self.loading.clone()), update_difficulties)
            .add_systems(OnEnter(self.running.clone()), dummy)
            .add_systems(
                Update,
                (
                    spawn_mobs.before(crate::start_scaling),
                    move_mobs.before(apply_velocity),
                    damage_mobs,
                    run_waves,
                )
                    .run_if(
                        resource_exists::<LoadedLevel>().and_then(in_state(self.running.clone())),
                    ),
            )
            .add_systems(
                Update,
                (
                    spawn_wave.run_if(resource_exists::<LoadedLevel>()),
                    add_starting_money.run_if(resource_exists_and_changed::<Difficulty>()),
                )
                    .run_if(
                        resource_exists::<Difficulty>().and_then(in_state(self.running.clone())),
                    ),
            )
            .register_asset_reflect::<Difficulty>();
    }
}

const MOB_Z: f32 = 0.5;

fn dummy(
    mut commands: Commands,
    difficulties: Res<Assets<Difficulty>>,
    mob_infos: Res<MobInfos>,
    mut e: EventWriter<SpawnWave>,
) {
    let Some(diff) = mob_infos
        .difficulties
        .get("difficulties/normal.difficulty.ron")
    else {
        return;
    };
    let Some(difficulty) = difficulties.get(diff) else {
        return;
    };

    commands.insert_resource(difficulty.clone());
    e.send(SpawnWave(0));
}

#[derive(Debug, Component, Default, Reflect)]
struct WaveTimer(f32);

#[derive(Debug, Event, Default)]
struct SpawnWave(usize);

fn spawn_wave(
    difficulty: Res<Difficulty>,
    mut events: EventReader<SpawnWave>,
    mut commands: Commands,
) {
    for SpawnWave(index) in events.read() {
        let Some(wave) = difficulty.waves.get(*index) else {
            continue;
        };
        commands.spawn((
            LevelEntity,
            wave.clone(),
            WaveIndex(*index),
            WaveTimer::default(),
        ));
    }
}

fn run_waves(
    mut waves: Query<(Entity, &Wave, &mut WaveTimer, &WaveIndex)>,
    time: Res<Time>,
    mut spawn_events: EventWriter<SpawnMob>,
    mut commands: Commands,
) {
    for (entity, wave, mut wave_timer, wave_index) in &mut waves {
        let last_frame = wave_timer.0;
        wave_timer.0 += time.delta_seconds() * 1000.0;
        let now = wave_timer.0;

        let mut finished = true;

        let mut t_spawn = 0.0;
        for spawn in &wave.spawns {
            t_spawn += spawn.wait_ms;
            if t_spawn >= now {
                finished = false;
                break;
            }

            'group: for group in &spawn.groups {
                let mut t_group = t_spawn;

                for _ in 0..group.amount {
                    if t_group >= now {
                        finished = false;
                        continue 'group;
                    }

                    if t_group >= last_frame {
                        spawn_events.send(SpawnMob {
                            mob: group.mob.clone(),
                            wave: wave_index.0,
                        });
                    }

                    t_group += group.wait_ms;
                }
            }
        }
        if finished {
            commands.entity(entity).remove::<WaveTimer>();
        }
    }
}

fn damage_mobs(
    level: Res<LoadedLevel>,
    mut mobs: Query<(Entity, &mut Health)>,
    time: Res<Time>,
    mut commands: Commands,
) {
    let dmg = time.delta_seconds() * level.damage;
    for (entity, mut mob) in &mut mobs {
        mob.0 -= dmg;
        if mob.0 <= 0.0 {
            commands.entity(entity).despawn();
        }
    }
}

fn add_starting_money(mut money_events: EventWriter<AddMoneyEvent>, difficulty: Res<Difficulty>) {
    money_events.send(AddMoneyEvent(difficulty.starting_value));
}

fn update_difficulties(mut difficulties: ResMut<Assets<Difficulty>>) {
    for (_, diff) in difficulties.iter_mut() {
        for wave in &mut diff.waves {
            for spawn in &mut wave.spawns {
                for group in &mut spawn.groups {
                    let Some(mob) = diff.mobs.get(&group.ty) else {
                        continue;
                    };
                    group.mob = mob.clone();
                }
            }
        }
    }
}

#[derive(Debug, Event)]
struct SpawnMob {
    mob: Mob,
    wave: usize,
}

fn spawn_mobs(
    mut spawns: EventReader<SpawnMob>,
    mut commands: Commands,
    level: Res<LoadedLevel>,
    level_root: Query<Entity, With<LevelOffset>>,
    assets: Res<GeneralAssets>,
) {
    let atlas = &assets.atlas;
    let pos = level.path.get(0).cloned().unwrap_or_default();

    let root = match level_root.get_single() {
        Ok(root) => root,
        Err(bevy::ecs::query::QuerySingleError::MultipleEntities(_)) => {
            warn!("Multiple Levelroots!");
            return;
        }
        Err(_) => {
            return;
        }
    };

    for SpawnMob { mob, wave } in spawns.read() {
        info!("spawning");
        commands
            .spawn((
                WaveIndex(*wave),
                Value(mob.value),
                Health(mob.health, mob.health),
                Speed(mob.speed),
                PathDistance(0.0),
                Velocity(Vec2::ZERO),
                Collider::ball(30.0),
                SpriteSheetBundle {
                    transform: Transform::from_xyz(pos.x, pos.y, MOB_Z)
                        .with_scale(Vec2::splat(mob.size).extend(1.0)),
                    sprite: TextureAtlasSprite {
                        index: mob.index,
                        ..default()
                    },
                    texture_atlas: atlas.clone(),
                    ..default()
                },
                LevelEntity,
                Name::new("Mob"),
            ))
            .set_parent(root);
    }
}

fn find_path_pos(path: &Vec<Vec2>, pos: f32) -> (bool, Vec2) {
    let mut last = None;
    let mut length_remaining = pos;
    for p in path {
        if let Some(last) = last {
            let segment: Vec2 = *p - last;
            let len = segment.length();

            if length_remaining <= len {
                let rel_len = length_remaining / len;

                return (false, last + (rel_len * segment));
            }
            length_remaining -= len;
        }

        last = Some(*p);
    }

    (true, last.unwrap_or_default())
}

fn move_mobs(
    mut mobs: Query<(
        Entity,
        &mut PathDistance,
        &Speed,
        &Value,
        &mut Transform,
        &mut Velocity,
    )>,
    level: Res<LoadedLevel>,
    time: Res<Time>,
    mut money_events: EventWriter<AddMoneyEvent>,
    mut commands: Commands,
) {
    for (entity, mut distance, speed, value, mut transform, mut velocity) in &mut mobs {
        let (finished, p) = find_path_pos(&level.path, distance.0);
        let movement = -(p - transform.translation.truncate()).angle_between(Vec2::X);
        velocity.0 = (p - transform.translation.xy()) / time.delta_seconds();

        transform.rotation = Quat::from_axis_angle(Vec3::Z, movement);

        if finished {
            money_events.send(AddMoneyEvent(value.0));
            commands.entity(entity).despawn();
        }

        distance.0 += speed.0 * time.delta_seconds();
    }
}

#[derive(Debug, Component, Reflect)]
pub struct PathDistance(pub f32);

#[derive(Debug, AssetCollection, Resource, Reflect, Default)]
#[reflect(Resource)]
struct MobInfos {
    #[asset(path = "difficulties", collection(typed, mapped))]
    difficulties: HashMap<String, Handle<Difficulty>>,
}

#[derive(Debug, Clone, Deserialize, Reflect, TypeUuid, Resource, Default, Asset)]
#[reflect(Resource)]
#[uuid = "3ceedcd5-2b00-429a-b54c-ecd708df38f7"]
struct Difficulty {
    starting_value: u32,
    waves: Vec<Wave>,
    mobs: HashMap<String, Mob>,
}

#[derive(Debug, Deserialize, Reflect, Component, Clone)]
struct Wave {
    spawns: Vec<Spawn>,
    value: u32,
}

#[derive(Debug, Deserialize, Reflect, Clone)]
struct Spawn {
    wait_ms: f32,
    groups: Vec<Group>,
}

#[derive(Debug, Deserialize, Reflect, Clone)]
struct Group {
    wait_ms: f32,
    #[serde(rename = "type")]
    ty: String,
    #[serde(skip)]
    mob: Mob,
    amount: u8,
}

#[derive(Debug, Deserialize, Reflect, Default, Clone)]
struct Mob {
    index: usize,
    speed: f32,
    health: f32,
    value: u32,
    size: f32,
}

#[derive(Debug, Deserialize, Reflect, Component, Resource, Clone)]
struct WaveIndex(usize);
#[derive(Debug, Deserialize, Reflect, Component, Clone)]
pub struct Health(pub f32, pub f32);
#[derive(Debug, Deserialize, Reflect, Component, Clone)]
struct Value(u32);
#[derive(Debug, Deserialize, Reflect, Component, Clone)]
struct Speed(f32);
