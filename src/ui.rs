use bevy::{
    diagnostic::{DiagnosticsStore, FrameTimeDiagnosticsPlugin},
    prelude::*,
    utils::HashMap,
};
use bevy_asset_loader::prelude::{AssetCollection, LoadingStateAppExt};

mod hud;

pub struct UiPlugin<S> {
    loading: S,
    running: S,
}

impl<S> UiPlugin<S> {
    pub fn new(loading: S, running: S) -> Self {
        Self { loading, running }
    }
}

impl<S: States + Clone> Plugin for UiPlugin<S> {
    fn finish(&self, app: &mut App) {
        app.add_collection_to_loading_state::<_, UiAssets>(self.loading.clone());
    }
    fn build(&self, app: &mut App) {
        app.register_type::<UiAssets>()
            .add_plugins((
                FrameTimeDiagnosticsPlugin,
                hud::HudPlugin::new(self.running.clone()),
            ))
            .add_systems(Startup, setup_fps_hud)
            .add_systems(Update, fpstext_update_system);
    }
}

#[derive(Debug, AssetCollection, Resource, Reflect, Default)]
#[reflect(Resource)]
struct UiAssets {
    #[asset(path = "fonts", collection(typed, mapped))]
    fonts: HashMap<String, Handle<Font>>,
}

#[derive(Debug, Component)]
struct FpsText;

fn setup_fps_hud(mut commands: Commands) {
    commands.spawn((
        TextBundle::from_section(" FPS", TextStyle { ..default() }).with_style(Style {
            position_type: PositionType::Absolute,
            ..default()
        }),
        FpsText,
        Name::new("FpsText"),
    ));
}

fn fpstext_update_system(
    diagnostics: Res<DiagnosticsStore>,
    mut query: Query<&mut Text, With<FpsText>>,
) {
    for mut text in &mut query {
        if let Some(fps) = diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) {
            if let Some(value) = fps.smoothed() {
                text.sections[0].value = format!("{value:.2} FPS");
            }
        }
    }
}
