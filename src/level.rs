use bevy::{prelude::*, reflect::TypeUuid, utils::HashMap};
use bevy_asset_loader::prelude::*;
use bevy_common_assets::ron::RonAssetPlugin;
use bevy_mod_picking::{
    prelude::{Click, On, Pickable, Pointer},
    PickableBundle,
};
use serde::Deserialize;

use crate::{
    tower::TrySpawnTower, GeneralAssets, LevelEntity, LevelOffset, LevelRoot, LoadedLevel,
};

pub struct LevelPlugin<S> {
    loading: S,
    running: S,
}

impl<S> LevelPlugin<S> {
    pub fn new(loading: S, running: S) -> Self {
        Self { loading, running }
    }
}

impl<S: States + Clone> Plugin for LevelPlugin<S> {
    fn finish(&self, app: &mut App) {
        app.add_collection_to_loading_state::<_, Levels>(self.loading.clone());
    }

    fn build(&self, app: &mut App) {
        app.add_plugins(RonAssetPlugin::<Level>::new(&["level.ron"]))
            .register_type::<Levels>()
            .register_type::<Level>()
            .add_event::<LoadLevel>()
            .register_asset_reflect::<Level>()
            .add_systems(OnEnter(self.running.clone()), dummy)
            .add_systems(
                Update,
                (
                    load_level,
                    unload_level.run_if(resource_changed_or_removed::<LoadedLevel>()),
                    render_level
                        .run_if(resource_exists_and_changed::<LoadedLevel>())
                        .after(unload_level),
                )
                    .run_if(in_state(self.running.clone())),
            );
    }
}

const TOWER_PAD_Z: f32 = 0.1;
const TOWER_PAD_INDEX: usize = 15;

fn dummy(levels: Res<Levels>, mut writer: EventWriter<LoadLevel>) {
    let s = "levels/data/level1.level.ron".to_string();
    let Some(l) = levels.levels.get(&s) else {
        return;
    };

    writer.send(LoadLevel(l.clone()));
}

fn unload_level(mut commands: Commands, level_root: Query<Entity, With<LevelEntity>>) {
    for root in &level_root {
        commands.entity(root).despawn_recursive();
    }
    info!("Unloading Level")
}

fn load_level(
    mut level_events: EventReader<LoadLevel>,
    level_list: Res<Levels>,
    levels: Res<Assets<Level>>,
    mut commands: Commands,
) {
    let mut level = None;
    for ev in level_events.read() {
        level = Some(&ev.0);
    }
    let Some(level) = level else {
        return;
    };
    let Some(level) = levels.get(level) else {
        warn!("Level was not loaded");
        return;
    };
    let Some(bg_image) = level_list.images.get(&level.background_image) else {
        return;
    };

    let path = level.path.clone();

    info!("Loading Level!");
    commands.insert_resource(LoadedLevel {
        bg_image: bg_image.clone(),
        path,
        size: level.level_size,
        grids: level.grids.clone(),
        grid_scale: level.grid_scale,
        damage: level.damage,
    });
}

fn render_level(
    mut commands: Commands,
    loaded_level: Res<LoadedLevel>,
    assets: Res<GeneralAssets>,
) {
    let atlas = &assets.atlas;
    commands
        .spawn((
            LevelEntity,
            SpriteBundle {
                texture: loaded_level.bg_image.clone(),
                ..default()
            },
            LevelRoot,
            Name::new("Level"),
            Pickable::IGNORE,
        ))
        .with_children(|p| {
            p.spawn((
                LevelOffset,
                TransformBundle {
                    local: Transform::from_translation(
                        ((loaded_level.size * Vec2::new(-1.0, 1.0)) / 2.0).extend(0.0),
                    )
                    .with_scale(Vec3::new(1.0, -1.0, 1.0)),
                    ..default()
                },
                VisibilityBundle::default(),
                Name::new("LevelOffset"),
            ))
            .with_children(|p| {
                for x in &loaded_level.grids {
                    p.spawn((
                        Name::new("Pad"),
                        SpriteSheetBundle {
                            transform: Transform::from_xyz(x.x, x.y, TOWER_PAD_Z)
                                .with_scale(Vec2::splat(loaded_level.grid_scale).extend(1.0)),
                            sprite: TextureAtlasSprite {
                                index: TOWER_PAD_INDEX,
                                ..default()
                            },
                            texture_atlas: atlas.clone(),
                            ..default()
                        },
                        PickableBundle {
                            pickable: Pickable {
                                should_block_lower: true,
                                should_emit_events: true,
                            },
                            ..default()
                        },
                        PadTag,
                        On::<Pointer<Click>>::send_event::<TrySpawnTower>(),
                    ));
                }
            });
        });
}

#[derive(Debug, Component)]
pub struct PadTag;

#[derive(Debug, Event)]
struct LoadLevel(Handle<Level>);

#[derive(AssetCollection, Resource, Reflect, Default)]
#[reflect(Resource)]
struct Levels {
    #[asset(path = "levels/bg", collection(typed, mapped))]
    images: HashMap<String, Handle<Image>>,
    #[asset(path = "levels/data", collection(typed, mapped))]
    levels: HashMap<String, Handle<Level>>,
}

#[derive(Debug, Deserialize, TypeUuid, Reflect, Asset)]
#[uuid = "e88f6473-45c1-457c-a4a4-ff1d97ab458d"]
struct Level {
    level_size: Vec2,
    background_image: String,
    grid_scale: f32,
    path: Vec<Vec2>,
    grids: Vec<Vec2>,
    damage: f32,
}
