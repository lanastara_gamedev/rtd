use std::collections::BTreeMap;

use bevy::{
    prelude::*,
    reflect::{Reflect, TypeUuid},
    utils::HashMap,
};
use bevy_asset_loader::prelude::{AssetCollection, LoadingStateAppExt};
use bevy_common_assets::ron::RonAssetPlugin;
use bevy_mod_picking::prelude::{Click, ListenerInput, Pointer};
use serde::Deserialize;

use crate::{
    level::PadTag,
    mob::{Health, PathDistance},
    physics_marker, Collider, Despawn, GeneralAssets, LevelOffset, Velocity,
};

pub struct TowerPlugin<S> {
    loading: S,
    running: S,
}

impl<S> TowerPlugin<S> {
    pub fn new(loading: S, running: S) -> Self {
        Self { loading, running }
    }
}
impl<S: States + Clone> Plugin for TowerPlugin<S> {
    fn finish(&self, app: &mut App) {
        app.add_collection_to_loading_state::<_, TowerInfos>(self.loading.clone());
    }

    fn build(&self, app: &mut App) {
        app.add_plugins(RonAssetPlugin::<Tower>::new(&["tower.ron"]))
            .add_event::<TrySpawnTower>()
            .add_event::<ProjectileHitEvent>()
            .register_type::<TowerInfos>()
            .register_type::<TurretComponent>()
            .register_type::<TargetMode>()
            .register_type::<SelectedTower>()
            .register_asset_reflect::<Tower>()
            .add_systems(OnEnter(self.running.clone()), dummy_setup)
            .add_systems(
                Update,
                (
                    spawn_towers,
                    tower_shoot,
                    bullet_explode,
                    bullet_physics.before(physics_marker),
                )
                    .run_if(in_state(self.running.clone())),
            );
    }
}

fn dummy_setup(mut commands: Commands, tower_infos: Res<TowerInfos>) {
    let Some((_, tower)) = tower_infos.towers.iter().next() else {
        return;
    };
    commands.insert_resource(SelectedTower(tower.clone()));
}

#[derive(Event)]
pub struct TrySpawnTower(Entity);

impl From<ListenerInput<Pointer<Click>>> for TrySpawnTower {
    fn from(value: ListenerInput<Pointer<Click>>) -> Self {
        TrySpawnTower(value.target)
    }
}

fn tower_shoot(
    mut turrets: Query<(&mut TurretComponent, &Parent, &mut Transform), Without<TargetMode>>,
    towers: Query<(&TargetMode, &Parent), Without<TurretComponent>>,
    pads: Query<&Transform, (With<PadTag>, Without<TurretComponent>)>,
    mobs: Query<(&PathDistance, &Health, &Transform), Without<TurretComponent>>,
    time: Res<Time>,
    mut commands: Commands,
    level_root: Query<Entity, With<LevelOffset>>,
    assets: Res<GeneralAssets>,
) {
    let atlas = &assets.atlas;
    let root = match level_root.get_single() {
        Ok(root) => root,
        Err(bevy::ecs::query::QuerySingleError::MultipleEntities(_)) => {
            warn!("Multiple Levelroots!");
            return;
        }
        Err(_) => {
            return;
        }
    };

    for (mut turret, tower_id, mut turret_transform) in turrets.iter_mut() {
        let Ok((target_mode, pad_id)) = towers.get(tower_id.get()) else {
            warn!("Turret without a tower");
            continue;
        };

        let Ok(tower_transform) = pads.get(pad_id.get()) else {
            warn!("Turret without a tower");
            continue;
        };

        let tower_pos = tower_transform.translation;
        let rotate = turret.rotate;
        for attack in turret.attacks.iter_mut() {
            if attack.t < attack.attack.delay {
                attack.t += time.delta_seconds();
                continue;
            }

            match attack.attack.target {
                AttackTargetType::Tower => todo!(),
                AttackTargetType::Mob(distance) => {
                    let max_distance_squared = distance.powi(2);

                    let mut value = match target_mode {
                        TargetMode::First | TargetMode::Weak | TargetMode::Near => f32::MAX,
                        TargetMode::Last | TargetMode::Strong | TargetMode::Far => 0.0,
                    };
                    let mut target = None;

                    for (distance, health, transform) in mobs.iter() {
                        let pos = transform.translation;
                        let distance_squared = pos.distance_squared(tower_pos);
                        if distance_squared <= max_distance_squared {
                            match target_mode {
                                TargetMode::First if distance.0 < value => {
                                    value = distance.0;
                                    target = Some(pos);
                                }
                                TargetMode::Last if distance.0 > value => {
                                    value = distance.0;
                                    target = Some(pos);
                                }
                                TargetMode::Strong if health.0 > value => {
                                    value = health.0;
                                    target = Some(pos);
                                }
                                TargetMode::Weak if health.0 < value => {
                                    value = health.0;
                                    target = Some(pos);
                                }
                                TargetMode::Near if distance_squared < value => {
                                    value = distance_squared;
                                    target = Some(pos);
                                }
                                TargetMode::Far if distance_squared > value => {
                                    value = distance_squared;
                                    target = Some(pos);
                                }
                                _ => (),
                            }
                        }
                    }
                    if let Some(pos) = target {
                        if rotate {
                            let dir = (pos - tower_pos).xy();
                            let angle = (-Vec2::Y).angle_between(dir);
                            turret_transform.rotation = Quat::from_rotation_z(angle);
                        }

                        attack.t = 0.0;

                        let direction = (pos.xy() - tower_pos.xy()).normalize();
                        let velocity = direction * attack.attack.projectile.speed;

                        let despawn =
                            attack.attack.projectile.distance / attack.attack.projectile.speed;

                        commands
                            .spawn((
                                Name::new("Bullet"),
                                attack.attack.projectile.clone(),
                                Velocity(velocity),
                                Collider::ball(1.0),
                                Despawn(despawn),
                                SpriteSheetBundle {
                                    transform: Transform::from_xyz(tower_pos.x, tower_pos.y, 0.6),
                                    sprite: TextureAtlasSprite {
                                        index: attack.attack.projectile.index,
                                        flip_y: true,
                                        ..default()
                                    },
                                    texture_atlas: atlas.clone(),
                                    ..default()
                                },
                            ))
                            .set_parent(root);
                    }
                }
                AttackTargetType::Position(_) => todo!(),
                AttackTargetType::Multiple(_) => todo!(),
            }
        }
    }
}

fn bullet_physics(
    bullets: Query<(Entity, &Collider, &Velocity, &Transform), With<Projectile>>,
    mobs: Query<(Entity, &Collider, &Velocity, &Transform), (Without<Projectile>, With<Health>)>,
    time: Res<Time>,
    mut events: EventWriter<ProjectileHitEvent>,
) {
    let mut collisions: BTreeMap<CFloat, Vec<(Entity, Entity, parry2d::na::Point2<f32>)>> =
        BTreeMap::new();

    for (mob, mob_collider, mob_velocity, mob_transform) in mobs.iter() {
        let pos1 = make_isometry(mob_transform);
        let g1 = mob_collider.shape();
        let vel1 = make_velocity(mob_velocity);
        for (bullet, bullet_collider, bullet_velocity, bullet_transform) in bullets.iter() {
            let pos2 = make_isometry(bullet_transform);
            let g2 = bullet_collider.shape();
            let vel2 = make_velocity(bullet_velocity);

            let Ok(Some(toi)) = parry2d::query::time_of_impact(
                &pos1,
                &vel1,
                g1,
                &pos2,
                &vel2,
                g2,
                time.delta_seconds(),
                true,
            ) else {
                continue;
            };

            let Ok(t) = CFloat::try_from(toi.toi) else {
                continue;
            };
            if let Some(v) = collisions.get_mut(&t) {
                v.push((mob, bullet, toi.witness2));
            } else {
                collisions.insert(t, vec![(mob, bullet, toi.witness2)]);
            }
        }
    }

    let i = collisions.into_iter().flat_map(|(_distance, collisions)| {
        collisions.into_iter().map(|(mob, bullet, position)| {
            let position = Vec2::new(position.x, position.y);
            ProjectileHitEvent {
                mob,
                bullet,
                position,
            }
        })
    });
    events.send_batch(i)
}

#[derive(PartialEq)]
struct CFloat(f32);

impl Eq for CFloat {}

impl PartialOrd for CFloat {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl Ord for CFloat {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0.partial_cmp(&other.0).unwrap()
    }
}

impl TryFrom<f32> for CFloat {
    type Error = ();

    fn try_from(value: f32) -> Result<Self, Self::Error> {
        if value.is_finite() {
            Ok(Self(value))
        } else {
            Err(())
        }
    }
}

fn make_velocity(velocity: &Velocity) -> parry2d::na::Vector2<f32> {
    parry2d::na::Vector2::new(velocity.0.x, velocity.0.y)
}

fn make_isometry(
    mob_transform: &Transform,
) -> parry2d::na::Isometry<f32, parry2d::na::Unit<parry2d::na::Complex<f32>>, 2> {
    parry2d::na::Isometry2::new(
        parry2d::na::Vector2::new(mob_transform.translation.x, mob_transform.translation.y),
        mob_transform.rotation.z,
    )
}

fn bullet_explode(
    mut ev: EventReader<ProjectileHitEvent>,
    mut bullets: Query<(&mut Projectile, Entity)>,
    mut mobs: Query<(&mut Health, &Transform), Without<Projectile>>,
    mut commands: Commands,
) {
    for ProjectileHitEvent {
        bullet,
        mob,
        position,
    } in ev.read()
    {
        let Ok((mut bullet_projectile, bullet_entity)) = bullets.get_mut(bullet.clone()) else {
            continue;
        };

        match bullet_projectile.target {
            ProjectileTarget::Single => {
                let Ok((mut mob_health, _mob_transform)) = mobs.get_mut(mob.clone()) else {
                    continue;
                };

                let Some(healing) = bullet_projectile.healing else {
                    continue;
                };

                mob_health.0 += healing;

                commands.entity(bullet_entity).despawn();

                return;
            }
            ProjectileTarget::Chain(_, _) => todo!(),
            ProjectileTarget::Area(_) => todo!(),
        }
    }
}

#[derive(Debug, Event)]
struct ProjectileHitEvent {
    bullet: Entity,
    mob: Entity,
    position: Vec2,
}

fn spawn_towers(
    mut events: EventReader<TrySpawnTower>,
    pads: Query<Entity, (With<PadTag>, Without<Children>)>,
    mut commands: Commands,
    selected_tower: Option<Res<SelectedTower>>,
    towers: Res<Assets<Tower>>,
    assets: Res<GeneralAssets>,
) {
    let atlas = &assets.atlas;
    let Some(selected_tower) = selected_tower else {
        events.clear();
        return;
    };
    let Some(tower) = towers.get(selected_tower.0.clone()) else {
        events.clear();
        return;
    };

    for event in events.read() {
        if !pads.contains(event.0) {
            continue;
        }

        commands
            .spawn((
                Name::new("Tower"),
                SpriteSheetBundle {
                    sprite: TextureAtlasSprite {
                        index: 181,
                        flip_y: true,
                        ..default()
                    },
                    texture_atlas: atlas.clone(),
                    ..default()
                },
                tower.target_modes[0].clone(),
            ))
            .with_children(|p| {
                p.spawn((
                    Name::new("Turret"),
                    SpriteSheetBundle {
                        transform: Transform::from_xyz(0.0, 0.0, 0.1),
                        sprite: TextureAtlasSprite {
                            index: tower.turret.index,
                            flip_y: true,
                            ..default()
                        },
                        texture_atlas: atlas.clone(),
                        ..default()
                    },
                    TurretComponent {
                        rotate: tower.turret.rotate,
                        attacks: tower
                            .turret
                            .attacks
                            .iter()
                            .map(TurretAttack::from)
                            .collect(),
                    },
                ));
            })
            .set_parent(event.0);
    }
}

#[derive(Debug, Component, Reflect)]
struct TurretComponent {
    rotate: bool,
    attacks: Vec<TurretAttack>,
}

#[derive(Debug, Reflect)]
struct TurretAttack {
    t: f32,
    attack: Attack,
}

impl From<&Attack> for TurretAttack {
    fn from(value: &Attack) -> Self {
        Self {
            t: value.delay,
            attack: value.clone(),
        }
    }
}

#[derive(Debug, Clone, Reflect, Resource, Default, Asset)]
#[reflect(Resource)]
struct SelectedTower(Handle<Tower>);

#[derive(Debug, AssetCollection, Resource, Reflect, Default)]
#[reflect(Resource)]
struct TowerInfos {
    #[asset(path = "towers", collection(typed, mapped))]
    towers: HashMap<String, Handle<Tower>>,
}

#[derive(Debug, Clone, Deserialize, Reflect, TypeUuid, Resource, Default, Asset)]
#[reflect(Resource)]
#[uuid = "3a5c86b7-06eb-4698-ad6f-0b1453193865"]
struct Tower {
    cost: u32,
    target_modes: Vec<TargetMode>,
    turret: Turret,
}

#[derive(Debug, Deserialize, Reflect, Clone, Default)]
struct Turret {
    index: usize,
    rotate: bool,
    attacks: Vec<Attack>,
}

#[derive(Debug, Deserialize, Reflect, Clone, Component)]
enum TargetMode {
    First,
    Last,
    Strong,
    Weak,
    Near,
    Far,
}

#[derive(Debug, Deserialize, Reflect, Clone)]
struct Attack {
    delay: f32,
    target: AttackTargetType,
    projectile: Projectile,
}

#[derive(Debug, Deserialize, Reflect, Clone)]
enum AttackTargetType {
    Tower,
    Mob(/*distance*/ f32),
    Position(Vec2),
    Multiple(Multiple),
}

#[derive(Debug, Deserialize, Reflect, Clone)]
struct Multiple {
    angle: Option<(f32, f32)>,
    count: u8,
}

#[derive(Debug, Deserialize, Reflect, Clone, Component)]
struct Projectile {
    target: ProjectileTarget,
    index: usize,
    speed: f32,
    distance: f32,
    #[serde(default)]
    healing: Option<f32>,
    #[serde(default)]
    max_healing_per_target: Option<f32>,
    fallback_target: bool,
}

#[derive(Debug, Deserialize, Reflect, Clone)]
enum ProjectileTarget {
    Single,
    Chain(u8, f32),
    Area(f32),
}
